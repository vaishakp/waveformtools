************
Simulations
************

This is a class to conveniently access, handle and retrieve information and data from Numerical Relativity simulations. 

Presently, we support the EinsteinToolkit data. Support for SpEC will appear soon.


The `sim` class provides a basic information and data storage container functionality and methods for handling the NR data. Presently the class reads in attributes like 

1. component masses, 
2. spins, 
3. waveforms, 
4. time stepping, 
5. merger time, 
6. data duration, 
7. coordinate positions of the centroids of the black holes.
8. the gravitational waveform modes.
9. the Centre of mass  correction parameters.

and many more...

