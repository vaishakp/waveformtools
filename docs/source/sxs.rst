SXS specific tools
==================

The ``wavefortools.sxs`` submodule provides capabilities to automate preparations/processing of waveforms and simulation metadata.
It provides capabilities like
1. Find simulations
2. Parse their parameters. compute secondary params
3. Export parameters as Markdown tables
4. Find and prepare waveforms i.e. extrapolate and apply CoM corrections using scri