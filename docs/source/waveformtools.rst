*************
Waveformtools
*************


The `waveformtools.waveformtools` module is a toolkit to carryout various transformations of the waveforms. Some of its basic features include:

1. Alignment of waveforms in time, phase.
2. Normalization
3. Checking for data repetition
4. Interpolation and resampling
5. Data smootheninig.
6. Tapering.
7. Conversion to `pycbc` timeseties.
8. Check data continuity
9. Extraction of amplitudes and unwrapped phases.
10. A simple progress bar
11. A logger/print function with verbosity levels.
12. Various other functions to facilitate data analysis of Numerical relativity data.
