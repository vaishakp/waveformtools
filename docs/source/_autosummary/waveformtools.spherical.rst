waveformtools.spherical
=======================

.. automodule:: waveformtools.spherical

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      decompose_in_SWSHs
      quad_on_sphere
   
   

   
   
   

   
   
   



