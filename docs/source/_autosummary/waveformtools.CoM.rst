waveformtools.CoM
=================

.. automodule:: waveformtools.CoM

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      X_com_moments
      boost_waveform
      compute_com_alpha
      compute_com_beta
      compute_conformal_k
      compute_translation_alpha_modes
   
   

   
   
   

   
   
   



