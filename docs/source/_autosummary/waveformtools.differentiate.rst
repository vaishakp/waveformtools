waveformtools.differentiate
===========================

.. automodule:: waveformtools.differentiate

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      Chebyshev_differential
      Fourier_differential
      differentiate
      differentiate2
      differentiate3
      differentiate4
      differentiate5
      differentiate5_vec_nonumba
      differentiate5_vec_numba
      differentiate_cwaveform
   
   

   
   
   

   
   
   



