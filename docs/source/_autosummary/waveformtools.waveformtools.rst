waveformtools.waveformtools
===========================

.. automodule:: waveformtools.waveformtools

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      addzeros
      apxstartend
      bintp
      center
      cleandata
      cleandata_old
      coalignwfs
      coalignwfs2
      compute_chirp_mass
      compute_frequencies
      differentiate
      flatten
      get_centered_taxis
      get_starting_angular_frequency
      get_waveform_angular_frequency
      integrate_first_order
      interp_resam_wfs
      interpolate_wfs
      iscontinuous
      iscontinuous_old
      lengtheq
      load_obj
      low_cut_filter
      massratio
      match_wfs
      match_wfs_pycbc
      mavg
      message
      mode
      norm
      olap
      plot
      pmmatch_wfs
      progressbar
      removeNans
      removezeros
      resample
      resample_wfs
      roll
      save_obj
      shiftmatched
      shorten
      simplematch_wfs_old
      smoothen
      startend
      taper
      taper_tanh
      taperlengtheq
      totalmass
      unwrap_phase
      xtract_camp
      xtract_camp_phase
      xtract_cphase
   
   

   
   
   

   
   
   



