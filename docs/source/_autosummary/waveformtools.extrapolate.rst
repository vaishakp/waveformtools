waveformtools.extrapolate
=========================

.. automodule:: waveformtools.extrapolate

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      r_to_ra_conversion
      waveextract_to_inf_perturbative_one_order
      waveextract_to_inf_perturbative_two_order
      waveextract_to_inf_perturbative_twop5_order
   
   

   
   
   

   
   
   



