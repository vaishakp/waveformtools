waveformtools.dataIO
====================

.. automodule:: waveformtools.dataIO

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_ell_max_from_file
      get_ell_max_from_keys
      load_RIT_Psi4_from_disk
      load_RIT_Strain_data_from_disk
      load_SpECTRE_data_from_disk
      load_SpEC_data_from_disk
      load_gen_data_from_disk
      save_modes_data_to_gen
   
   

   
   
   

   
   
   



