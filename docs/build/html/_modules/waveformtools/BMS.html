<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>waveformtools.BMS &mdash; waveformtools 2022-05-03 documentation</title>
      <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="../../_static/css/theme.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="../../_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="../../" id="documentation_options" src="../../_static/documentation_options.js"></script>
        <script src="../../_static/jquery.js"></script>
        <script src="../../_static/underscore.js"></script>
        <script src="../../_static/_sphinx_javascript_frameworks_compat.js"></script>
        <script src="../../_static/doctools.js"></script>
    <script src="../../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link href="../../_static/css/my_theme.css" rel="stylesheet" type="text/css">

</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search"  style="background: blue" >
            <a href="../../index.html" class="icon icon-home"> waveformtools
          </a>
              <div class="version">
                2022-05-03
              </div>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../../basic_features.html">Basic Features</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../api.html">Package API</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../simulations.html">Simulations</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../transforms.html">Transforms</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../waveforms.html">Waveforms</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../waveformtools.html">Waveformtools</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu"  style="background: blue" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../../index.html">waveformtools</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../../index.html" class="icon icon-home"></a></li>
          <li class="breadcrumb-item"><a href="../index.html">Module code</a></li>
          <li class="breadcrumb-item"><a href="../waveformtools.html">waveformtools</a></li>
      <li class="breadcrumb-item active">waveformtools.BMS</li>
      <li class="wy-breadcrumbs-aside">
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <h1>Source code for waveformtools.BMS</h1><div class="highlight"><pre>
<span></span><span class="sd">&quot;&quot;&quot; The implementation of BMS transformations on the waveforms. &quot;&quot;&quot;</span>


<span class="c1">#############################</span>
<span class="c1"># Imports</span>
<span class="c1">#############################</span>

<span class="kn">import</span> <span class="nn">numpy</span> <span class="k">as</span> <span class="nn">np</span>


<div class="viewcode-block" id="compute_conformal_k"><a class="viewcode-back" href="../../_autosummary/waveformtools.BMS.html#waveformtools.BMS.compute_conformal_k">[docs]</a><span class="k">def</span> <span class="nf">compute_conformal_k</span><span class="p">(</span><span class="n">vec_v</span><span class="p">,</span> <span class="n">theta</span><span class="p">,</span> <span class="n">phi</span><span class="p">,</span> <span class="n">spin_phase</span><span class="o">=</span><span class="mi">0</span><span class="p">):</span>
<span class="w">    </span><span class="sd">&quot;&quot;&quot;Compute the conformal factor for the boost transformation</span>
<span class="sd">            :math:`k = \\exp(-2i \\lambda) \\gamma^3</span>
<span class="sd">            (1 - \\mathbf{v} \\cdot \\mathbf{r})^3`</span>

<span class="sd">    Parameters</span>
<span class="sd">    ----------</span>
<span class="sd">    vec_v: list</span>
<span class="sd">                    The velocity vector.</span>

<span class="sd">    theta: float</span>
<span class="sd">                    The polar angle :math:`\\theta&#39; in radians.</span>

<span class="sd">    phi: float</span>
<span class="sd">                            The azimuthal angle :math:`\\phi&#39; in radians.</span>

<span class="sd">    spin_phase: float, optional</span>
<span class="sd">                             The spin phase :math:`\\lambda&#39;. Defaults to 0.</span>

<span class="sd">    Returns</span>
<span class="sd">    -------</span>
<span class="sd">    conformal_k:	float</span>
<span class="sd">                    The conformal factor for the</span>
<span class="sd">                    boost transformation as defined above.</span>
<span class="sd">    &quot;&quot;&quot;</span>

    <span class="c1"># unpack the velocity vector</span>
    <span class="n">vel_x</span><span class="p">,</span> <span class="n">vel_y</span><span class="p">,</span> <span class="n">vel_z</span> <span class="o">=</span> <span class="n">vec_v</span>

    <span class="c1"># magnitude of velocity</span>
    <span class="n">mag_v</span> <span class="o">=</span> <span class="n">np</span><span class="o">.</span><span class="n">sqrt</span><span class="p">(</span><span class="n">vel_x</span><span class="o">**</span><span class="mi">2</span> <span class="o">+</span> <span class="n">vel_y</span><span class="o">**</span><span class="mi">2</span> <span class="o">+</span> <span class="n">vel_z</span><span class="o">**</span><span class="mi">2</span><span class="p">)</span>
    <span class="c1"># compute the dot product</span>
    <span class="n">v_dot_r</span> <span class="o">=</span> <span class="n">np</span><span class="o">.</span><span class="n">sin</span><span class="p">(</span><span class="n">theta</span><span class="p">)</span> <span class="o">*</span> <span class="p">(</span><span class="n">vel_x</span> <span class="o">*</span> <span class="n">np</span><span class="o">.</span><span class="n">cos</span><span class="p">(</span><span class="n">phi</span><span class="p">)</span> <span class="o">+</span> <span class="n">vel_y</span> <span class="o">*</span> <span class="n">np</span><span class="o">.</span><span class="n">sin</span><span class="p">(</span><span class="n">phi</span><span class="p">))</span> <span class="o">+</span> <span class="n">vel_z</span> <span class="o">*</span> <span class="n">np</span><span class="o">.</span><span class="n">cos</span><span class="p">(</span><span class="n">theta</span><span class="p">)</span>

    <span class="c1"># Lorentz factor</span>
    <span class="n">gamma</span> <span class="o">=</span> <span class="mf">1.0</span> <span class="o">/</span> <span class="n">np</span><span class="o">.</span><span class="n">sqrt</span><span class="p">(</span><span class="mi">1</span> <span class="o">-</span> <span class="n">mag_v</span><span class="o">**</span><span class="mi">2</span><span class="p">)</span>

    <span class="c1"># spin_phase</span>
    <span class="n">spin_factor</span> <span class="o">=</span> <span class="n">np</span><span class="o">.</span><span class="n">exp</span><span class="p">(</span><span class="o">-</span><span class="mi">2</span> <span class="o">*</span> <span class="mi">1</span><span class="n">j</span> <span class="o">*</span> <span class="n">spin_phase</span><span class="p">)</span>

    <span class="c1"># Finally, the conformal factor</span>
    <span class="n">conformal_factor</span> <span class="o">=</span> <span class="n">spin_factor</span> <span class="o">*</span> <span class="n">np</span><span class="o">.</span><span class="n">power</span><span class="p">(</span><span class="n">gamma</span> <span class="o">*</span> <span class="p">(</span><span class="mi">1</span> <span class="o">-</span> <span class="n">v_dot_r</span><span class="p">),</span> <span class="mi">3</span><span class="p">)</span>

    <span class="k">return</span> <span class="n">conformal_factor</span></div>


<div class="viewcode-block" id="compute_supertransl_alpha"><a class="viewcode-back" href="../../_autosummary/waveformtools.BMS.html#waveformtools.BMS.compute_supertransl_alpha">[docs]</a><span class="k">def</span> <span class="nf">compute_supertransl_alpha</span><span class="p">(</span><span class="n">supertransl_alpha_modes</span><span class="p">,</span> <span class="n">theta</span><span class="p">,</span> <span class="n">phi</span><span class="p">):</span>
<span class="w">    </span><span class="sd">&quot;&quot;&quot;Compute the spherical Alpha supertranslation variable</span>
<span class="sd">    :math:`\\alpha(\\theta, \\phi)` given its modes. This method</span>
<span class="sd">    just multiplies the alpha modes with their corresponding spherical</span>
<span class="sd">    harmonic basis functions and returns the summed result.</span>


<span class="sd">    Parameters</span>
<span class="sd">    ----------</span>

<span class="sd">    supertransl_alpha_modes:	dict</span>
<span class="sd">                                A dictionary of lists, each sublist</span>
<span class="sd">                                containing the set of super-translation</span>
<span class="sd">                                modes corresponding to a particular</span>
<span class="sd">                                :math:`\\ell&#39;.</span>
<span class="sd">    theta:		float</span>
<span class="sd">                            The polar angle :math:`\\theta&#39;.</span>
<span class="sd">    phi:	float</span>
<span class="sd">                    The azimuthal angle :math:`\\phi&#39;.</span>

<span class="sd">    Returns</span>
<span class="sd">    --------</span>

<span class="sd">    supertransl_alpha_sphere:		func</span>
<span class="sd">                                    A function on the sphere</span>
<span class="sd">                                    (arguments :math:`\\theta&#39;, math:`\\phi&#39;).</span>


<span class="sd">    &quot;&quot;&quot;</span>

    <span class="c1"># For partial evaluation of functions</span>
    <span class="c1"># from functools import partial</span>
    <span class="nb">print</span><span class="p">(</span><span class="n">supertransl_alpha_modes</span><span class="o">.</span><span class="n">keys</span><span class="p">())</span>
    <span class="c1"># Find the extreme ell values.</span>
    <span class="n">keys_list</span> <span class="o">=</span> <span class="nb">sorted</span><span class="p">(</span><span class="nb">list</span><span class="p">(</span><span class="n">supertransl_alpha_modes</span><span class="o">.</span><span class="n">keys</span><span class="p">()))</span>

    <span class="c1"># ell_min = int(keys_list[0][1])</span>
    <span class="c1"># ell_max = int(keys_list[-1][1])</span>

    <span class="c1"># Import the Spherical Harmonic function</span>

    <span class="kn">from</span> <span class="nn">waveformtools.transforms</span> <span class="kn">import</span> <span class="n">Yslm</span>

    <span class="n">spin_weight</span> <span class="o">=</span> <span class="mi">0</span>
    <span class="c1"># Ylm = partial(Yslm, spin_weight=0)</span>
    <span class="c1"># The final function</span>
    <span class="n">supertransl_alpha_sphere</span> <span class="o">=</span> <span class="mi">0</span>

    <span class="n">theta</span> <span class="o">=</span> <span class="n">np</span><span class="o">.</span><span class="n">pi</span> <span class="o">/</span> <span class="mi">2</span>
    <span class="n">phi</span> <span class="o">=</span> <span class="mf">0.0</span>
    <span class="k">for</span> <span class="n">item</span> <span class="ow">in</span> <span class="n">keys_list</span><span class="p">:</span>
        <span class="n">ell</span> <span class="o">=</span> <span class="nb">int</span><span class="p">(</span><span class="n">item</span><span class="p">[</span><span class="mi">1</span><span class="p">])</span>
        <span class="k">for</span> <span class="n">m_index</span> <span class="ow">in</span> <span class="nb">range</span><span class="p">(</span><span class="mi">2</span> <span class="o">*</span> <span class="n">ell</span> <span class="o">+</span> <span class="mi">1</span><span class="p">):</span>
            <span class="n">emm</span> <span class="o">=</span> <span class="n">m_index</span> <span class="o">-</span> <span class="n">ell</span>
            <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;ell is&quot;</span><span class="p">,</span> <span class="n">ell</span><span class="p">,</span> <span class="nb">type</span><span class="p">(</span><span class="n">ell</span><span class="p">),</span> <span class="s2">&quot;emm is &quot;</span><span class="p">,</span> <span class="n">emm</span><span class="p">)</span>
            <span class="n">supertransl_alpha_sphere</span> <span class="o">+=</span> <span class="n">supertransl_alpha_modes</span><span class="p">[</span><span class="n">item</span><span class="p">][</span><span class="n">m_index</span><span class="p">]</span> <span class="o">*</span> <span class="n">Yslm</span><span class="p">(</span><span class="n">spin_weight</span><span class="p">,</span> <span class="n">ell</span><span class="p">,</span> <span class="n">emm</span><span class="p">,</span> <span class="n">theta</span><span class="p">,</span> <span class="n">phi</span><span class="p">)</span>

    <span class="k">return</span> <span class="n">supertransl_alpha_sphere</span></div>


<div class="viewcode-block" id="boost_waveform"><a class="viewcode-back" href="../../_autosummary/waveformtools.BMS.html#waveformtools.BMS.boost_waveform">[docs]</a><span class="k">def</span> <span class="nf">boost_waveform</span><span class="p">(</span><span class="n">unboosted_waveform</span><span class="p">,</span> <span class="n">conformal_factor</span><span class="p">):</span>
<span class="w">    </span><span class="sd">&quot;&quot;&quot;Boost the waveform given the unboosted waveform and the boost conformal factor.</span>

<span class="sd">    Parameters</span>
<span class="sd">    ----------</span>

<span class="sd">    non_boosted_waveform:	list</span>
<span class="sd">                            A list with a single floating point number</span>
<span class="sd">                            or a numpy array of the unboosted waveform.</span>
<span class="sd">                            The waveform can have angular as well as</span>
<span class="sd">                            time dimentions.</span>

<span class="sd">                            The nesting order should be that, given the</span>
<span class="sd">                            list `non_boosted_waveform&#39;, each item in the</span>
<span class="sd">                            list refers to an array defined on the sphere</span>
<span class="sd">                            at a particular time or frequency. The subitem</span>
<span class="sd">                            will have dimensions [ntheta, nphi].</span>



<span class="sd">    conformal_factor:		float/array</span>
<span class="sd">                            The conformal factor for the Lorentz transformation.</span>
<span class="sd">                            It may be a single floating point number or an array</span>
<span class="sd">                            on a spherical grid. The array will be of dimensions</span>
<span class="sd">                            [ntheta, nphi]</span>

<span class="sd">    gridinfo:		class instance</span>
<span class="sd">                    The class instance that contains the properties</span>
<span class="sd">                    of the spherical grid.</span>

<span class="sd">    &quot;&quot;&quot;</span>

    <span class="c1"># Find out if the unboosted waveform is a single number or defined on a spherical grid.</span>
    <span class="c1"># onepoint = isinstance(unboosted_waveform[0], float)</span>

    <span class="c1"># if not onepoint:</span>
    <span class="c1"># Get the spherical grid shape.</span>
    <span class="c1"># 	ntheta, nphi = np.array(unboosted_waveform[0]).shape</span>

    <span class="c1"># Compute the meshgrid for theta and phi.</span>
    <span class="c1"># theta, phi = gridinfo.meshgrid</span>

    <span class="c1"># A list to store the boosted waveform.</span>
    <span class="n">boosted_waveform</span> <span class="o">=</span> <span class="p">[]</span>

    <span class="k">for</span> <span class="n">item</span> <span class="ow">in</span> <span class="n">unboosted_waveform</span><span class="p">:</span>
        <span class="c1"># Compute the boosted waveform on the spherical grid on all the elements.</span>

        <span class="c1"># conformal_k_on_sphere = compute_conformal_k(vec_v, theta, phi)</span>
        <span class="n">boosted_waveform_item</span> <span class="o">=</span> <span class="n">conformal_factor</span> <span class="o">*</span> <span class="n">item</span>

        <span class="n">boosted_waveform</span><span class="o">.</span><span class="n">append</span><span class="p">(</span><span class="n">boosted_waveform_item</span><span class="p">)</span>

    <span class="k">return</span> <span class="n">boosted_waveform</span></div>
</pre></div>

           </div>
          </div>
          <footer>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2020, Vaishak Prasad.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script>
    <!-- Theme Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-XXXXXXXXXX"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-XXXXXXXXXX', {
          'anonymize_ip': false,
      });
    </script> 

</body>
</html>