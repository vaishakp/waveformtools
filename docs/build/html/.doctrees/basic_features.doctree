���;      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Basic Features�h]�h	�Text����Basic Features�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�p/home/vaishakprasad/Documents/IUCAA/Projects/Codes/custom_libraries/waveformtools/docs/source/basic_features.rst�hKubh	�	paragraph���)��}�(h��This is a python module for the handling and analysis of waveforms and data from Numerical Relativity codes, and for carrying out gravitational wave data analysis.�h]�h��This is a python module for the handling and analysis of waveforms and data from Numerical Relativity codes, and for carrying out gravitational wave data analysis.�����}�(hh1hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h��waveformtools is a numerical relativity data handling package that was written to aid the handling and analysis of numerical relativity data.�h]�h��waveformtools is a numerical relativity data handling package that was written to aid the handling and analysis of numerical relativity data.�����}�(hh?hh=hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h��This package contains implementations of customized algorithms and techniques.  Some of these contain the usage of existing python based library functions from pycbc, scipy, etc but effort has been made to keep these to a minimum.�h]�h��This package contains implementations of customized algorithms and techniques.  Some of these contain the usage of existing python based library functions from pycbc, scipy, etc but effort has been made to keep these to a minimum.�����}�(hhMhhKhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK	hhhhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(hX�  Handling of numerical relativity data, and retreiving specific information about the physical system.

  The class container and methods "sim" can load NR data into convenient lists and dictionaries, which can be used to     retrieve specific data/ information about the numerical simulation.

  This offers the following functionality, like retreiving:

  * Horizon masses, mass-ratios, and areas.

  * Horizon multipole moments.

  * Merger time/ formation time of common horizon.

  * The strain waveform.

  * The shear data of the dynamical horizons.

      * And computing/ extracting the Frequency, amplitude and phase of waveforms, etc.
�h]�(h.)��}�(h�eHandling of numerical relativity data, and retreiving specific information about the physical system.�h]�h�eHandling of numerical relativity data, and retreiving specific information about the physical system.�����}�(hhfhhdhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh`ubh	�block_quote���)��}�(hhh]�(h.)��}�(h��The class container and methods "sim" can load NR data into convenient lists and dictionaries, which can be used to     retrieve specific data/ information about the numerical simulation.�h]�h��The class container and methods “sim” can load NR data into convenient lists and dictionaries, which can be used to     retrieve specific data/ information about the numerical simulation.�����}�(hhyhhwhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhtubh.)��}�(h�9This offers the following functionality, like retreiving:�h]�h�9This offers the following functionality, like retreiving:�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhtubhZ)��}�(hhh]�(h_)��}�(h�(Horizon masses, mass-ratios, and areas.
�h]�h.)��}�(h�'Horizon masses, mass-ratios, and areas.�h]�h�'Horizon masses, mass-ratios, and areas.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hh�ubh_)��}�(h�Horizon multipole moments.
�h]�h.)��}�(h�Horizon multipole moments.�h]�h�Horizon multipole moments.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hh�ubh_)��}�(h�/Merger time/ formation time of common horizon.
�h]�h.)��}�(h�.Merger time/ formation time of common horizon.�h]�h�.Merger time/ formation time of common horizon.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hh�ubh_)��}�(h�The strain waveform.
�h]�h.)��}�(h�The strain waveform.�h]�h�The strain waveform.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hh�ubh_)��}�(h�The shear data of the dynamical horizons.

  * And computing/ extracting the Frequency, amplitude and phase of waveforms, etc.
�h]�(h.)��}�(h�)The shear data of the dynamical horizons.�h]�h�)The shear data of the dynamical horizons.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubhs)��}�(hhh]�hZ)��}�(hhh]�h_)��}�(h�PAnd computing/ extracting the Frequency, amplitude and phase of waveforms, etc.
�h]�h.)��}�(h�OAnd computing/ extracting the Frequency, amplitude and phase of waveforms, etc.�h]�h�OAnd computing/ extracting the Frequency, amplitude and phase of waveforms, etc.�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubah}�(h!]�h#]�h%]�h']�h)]��bullet��*�uh+hYhh,hKhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hrhh�ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h^hh�ubeh}�(h!]�h#]�h%]�h']�h)]�j,  j-  uh+hYhh,hKhhtubeh}�(h!]�h#]�h%]�h']�h)]�uh+hrhh`ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h^hh[hhhh,hNubh_)��}�(hX�  Handling of waveforms.

  A  basic class container for handling numerical waveforms.

  This offers functionality like:

  * loading data from hdf5 files.

  * basic information such as time step, time axis, data axis, etc.

  * extrapolating a numerical waveform recorded at a finite radius to null infinity (to be added soon).

  * integrating and differentiating waveforms in the frequency domain (to be added soon).


�h]�(h.)��}�(h�Handling of waveforms.�h]�h�Handling of waveforms.�����}�(hjR  hjP  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhjL  ubhs)��}�(hhh]�(h.)��}�(h�:A  basic class container for handling numerical waveforms.�h]�h�:A  basic class container for handling numerical waveforms.�����}�(hjc  hja  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK!hj^  ubh.)��}�(h�This offers functionality like:�h]�h�This offers functionality like:�����}�(hjq  hjo  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK#hj^  ubhZ)��}�(hhh]�(h_)��}�(h�loading data from hdf5 files.
�h]�h.)��}�(h�loading data from hdf5 files.�h]�h�loading data from hdf5 files.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK%hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj}  ubh_)��}�(h�@basic information such as time step, time axis, data axis, etc.
�h]�h.)��}�(h�?basic information such as time step, time axis, data axis, etc.�h]�h�?basic information such as time step, time axis, data axis, etc.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK'hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj}  ubh_)��}�(h�dextrapolating a numerical waveform recorded at a finite radius to null infinity (to be added soon).
�h]�h.)��}�(h�cextrapolating a numerical waveform recorded at a finite radius to null infinity (to be added soon).�h]�h�cextrapolating a numerical waveform recorded at a finite radius to null infinity (to be added soon).�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK)hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj}  ubh_)��}�(h�Xintegrating and differentiating waveforms in the frequency domain (to be added soon).


�h]�h.)��}�(h�Uintegrating and differentiating waveforms in the frequency domain (to be added soon).�h]�h�Uintegrating and differentiating waveforms in the frequency domain (to be added soon).�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK+hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj}  ubeh}�(h!]�h#]�h%]�h']�h)]�j,  j-  uh+hYhh,hK%hj^  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+hrhjL  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h^hh[hhhh,hNubh_)��}�(hXr  Tools for preparation/handling of numerical relativity data, like:

  * checking for discontinuity, removal of duplicated rows, and interpolating for missing rows in the data.

  * integration, differentiation of numerical data.

  * equalizing lengths of waveforms.

  * resampling

  * computing the norm, shifting/ rolling the data, etc.

  * Smoothening, tapering.

�h]�(h.)��}�(h�BTools for preparation/handling of numerical relativity data, like:�h]�h�BTools for preparation/handling of numerical relativity data, like:�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK/hj�  ubhs)��}�(hhh]�hZ)��}�(hhh]�(h_)��}�(h�hchecking for discontinuity, removal of duplicated rows, and interpolating for missing rows in the data.
�h]�h.)��}�(h�gchecking for discontinuity, removal of duplicated rows, and interpolating for missing rows in the data.�h]�h�gchecking for discontinuity, removal of duplicated rows, and interpolating for missing rows in the data.�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK1hj
  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�0integration, differentiation of numerical data.
�h]�h.)��}�(h�/integration, differentiation of numerical data.�h]�h�/integration, differentiation of numerical data.�����}�(hj(  hj&  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK3hj"  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�!equalizing lengths of waveforms.
�h]�h.)��}�(h� equalizing lengths of waveforms.�h]�h� equalizing lengths of waveforms.�����}�(hj@  hj>  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK5hj:  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�resampling
�h]�h.)��}�(h�
resampling�h]�h�
resampling�����}�(hjX  hjV  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK7hjR  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�5computing the norm, shifting/ rolling the data, etc.
�h]�h.)��}�(h�4computing the norm, shifting/ rolling the data, etc.�h]�h�4computing the norm, shifting/ rolling the data, etc.�����}�(hjp  hjn  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK9hjj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�Smoothening, tapering.

�h]�h.)��}�(h�Smoothening, tapering.�h]�h�Smoothening, tapering.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK;hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubeh}�(h!]�h#]�h%]�h']�h)]�j,  j-  uh+hYhh,hK1hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hrhj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h^hh[hhhh,hNubh_)��}�(h�{Tools for basic data analysis.

  * A match algorithms for matching two waveforms.

  * Binning and interpolation of data.
�h]�(h.)��}�(h�Tools for basic data analysis.�h]�h�Tools for basic data analysis.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK>hj�  ubhs)��}�(hhh]�hZ)��}�(hhh]�(h_)��}�(h�/A match algorithms for matching two waveforms.
�h]�h.)��}�(h�.A match algorithms for matching two waveforms.�h]�h�.A match algorithms for matching two waveforms.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK@hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj�  ubh_)��}�(h�#Binning and interpolation of data.
�h]�h.)��}�(h�"Binning and interpolation of data.�h]�h�"Binning and interpolation of data.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKBhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�j,  j-  uh+hYhh,hK@hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hrhj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h^hh[hhhh,hNubh_)��}�(h��Miscellaneous tools:

  * Progress bar to display progress of loops.

  * A custom print function with message prioritization.

  * Saving data to disk with protocol support (binary, text, etc.)�h]�(h.)��}�(h�Miscellaneous tools:�h]�h�Miscellaneous tools:�����}�(hj  hj
  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKDhj  ubhs)��}�(hhh]�hZ)��}�(hhh]�(h_)��}�(h�+Progress bar to display progress of loops.
�h]�h.)��}�(h�*Progress bar to display progress of loops.�h]�h�*Progress bar to display progress of loops.�����}�(hj$  hj"  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKFhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�5A custom print function with message prioritization.
�h]�h.)��}�(h�4A custom print function with message prioritization.�h]�h�4A custom print function with message prioritization.�����}�(hj<  hj:  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKHhj6  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubh_)��}�(h�>Saving data to disk with protocol support (binary, text, etc.)�h]�h.)��}�(hjP  h]�h�>Saving data to disk with protocol support (binary, text, etc.)�����}�(hjP  hjR  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKJhjN  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h^hj  ubeh}�(h!]�h#]�h%]�h']�h)]�j,  j-  uh+hYhh,hKFhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hrhj  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h^hh[hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]�j,  j-  uh+hYhh,hKhhhhubeh}�(h!]��basic-features�ah#]�h%]��basic features�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j�  j  s�	nametypes�}�j�  Nsh!}�j  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.